# ece488

The repo for image processing class.

Please check the report at [this link](https://gitlab.oit.duke.edu/tc233/ece488/blob/master/final-project/submit/ECE488_report.pdf)

Please check the source images in the submit folder.
